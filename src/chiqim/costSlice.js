import { createSlice } from "@reduxjs/toolkit";
const time = new Date()
const initialState = [
    {
        id: 1,
        userName: "Mark",
        amount: 1000,
        kassa: "kassa1",
        time: time.toISOString
    }
]
const costSlice = createSlice({
    name: "income",
    initialState,
    reducers: {
        addUserCost: (state, action) => {
            state.push(action.payload);
        },
        deleteCost: (state, action) => {
            const { id } = action.payload;
            const delInc = state.find(f => f.id == id)
            if (delInc) {
                return state.filter(newState => newState.id != id)
            }
        }
    }
})
export const { addUserCost, deleteCost } = costSlice.actions;
export default costSlice.reducer;