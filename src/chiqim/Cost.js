import React, { useState } from 'react';
import "../App.css"
import { useDispatch, useSelector } from 'react-redux';
import { addUserCost, deleteCost } from './costSlice';
const Cashbox = () => {
  const [showSidebar, setShowSidebar] = useState(false);
  const [editSidebar, setEditSidebar] = useState(false);
  const [selectUser, setSelectUser] = useState("");
  const [selectKassa, setSelectKassa] = useState("");
  const [amount, setAmount] = useState("");
  const [date, setDate] = useState();
  const UserCost = useSelector((state) => state.costSlice);
  const users = useSelector((state) => state.usersSlice);
  const cashbox = useSelector((state) => state.cashboxSLice);
  const dispatch = useDispatch()
  let editKassaIndex;
  const addCost = () => {
    dispatch(addUserCost({ id: UserCost[UserCost.length - 1].id + 1, userName: selectUser, amount, kassa: selectKassa, time: date }))
    setShowSidebar(false)
  }
  const onSelectChange = (optionSelected) => {
    setSelectUser(optionSelected.value)
  }
  const deletedCost = (id) => {
    dispatch(deleteCost({ id: id }))
  }
  const editKassaName = (index) => {
    editKassaIndex = index
    setEditSidebar(true)
  }
  return (
    <div className='w-100 d-flex justify-content-center mt-5'>
      <div className='w-75 border border-bold p-3'>
        <h1 className='text-center'>Chiqim</h1>
        <div className='d-flex justify-content-end'>
          <button className='btn btn-success' onClick={() => setShowSidebar(true)}>+Add</button>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Amount</th>
              <th scope="col">Kassa</th>
              <th scope="col">Edit</th>
              <th scope="col">Delete</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {
              UserCost.map((Cost, index) => {
                console.log(Cost);
                return (
                  <tr key={index}>
                    <th scope="row">{Cost.id}</th>
                    <td>{Cost.userName}</td>
                    <td>{Cost.amount}</td>
                    <td>{Cost.kassa}</td>
                    <td >
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square svg" viewBox="0 0 16 16">
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                      </svg>

                    </td>
                    <td>
                      <svg onClick={() => deletedCost(Cost.id)} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash svg" viewBox="0 0 16 16">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z" />
                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z" />
                      </svg>
                    </td>
                    <td>{Cost.time}</td>
                  </tr>
                )
              })
            }

          </tbody>
        </table>
      </div>
      {
        showSidebar && <div className='ms-5 border border-bold py-3 px-5 rounded'>
          <div className='border border-bold rounded'>
            <h4 className='text-start px-3'>Kirim qilish</h4>
          </div>
          <select class="form-select" aria-label="Default select example" value={selectUser} onChange={onSelectChange}>
            <option selected>Foydalanuvchini tanlang</option>
            {
              users.map((user, index) => {
                return <>
                  <option value={index}>{user.name}</option>
                </>
              })
            }
          </select>
          <select class="form-select" aria-label="Default select example" onChange={(e) => setSelectKassa(e.target.value)}>
            <option selected>Kassani tanlang</option>
            {
              cashbox.map((item, index) => {
                return <>
                  <option value={index}>{item.kassaName}</option>
                </>
              })
            }
          </select>
          <input type='text' placeholder='Miqdori' onChange={(e) => setAmount(e.target.value)} />
          <input type="date" id="date-input" name="date" onChange={(e) => setDate(e.target.value)} />
          <div>
            <button className='btn btn-danger' onClick={() => setShowSidebar(false)}>Chiqish</button> |
            <button className='btn btn-info' onClick={addCost}>Saqlash</button>
          </div>
        </div>
      }
      {
        editSidebar && <div className='ms-5 border border-bold py-3 px-5 rounded'>
          <div className='border border-bold rounded'>
            <h4 className='text-start px-3'>Kirimni o'zgartirish</h4>
          </div>
          <input placeholder='Miqdori ' className='my-3' />
          <div>
            <button className='btn btn-danger' onClick={() => setEditSidebar(false)}>Chiqish</button> |
            <button className='btn btn-info' >Saqlash</button>
          </div>
        </div>
      }
    </div>
  )
}

export default Cashbox