import { configureStore } from "@reduxjs/toolkit";
import cashboxSLice from "../kassa/cashboxSlice";
import usersSlice from "../users/usersSlice";
import incomeSlice from "../kirim/incomeSlice";
import costSlice from "../chiqim/costSlice";
const store= configureStore({
    reducer:{
        cashboxSLice,
        usersSlice,
        incomeSlice,
        costSlice
    }
})
export default store;