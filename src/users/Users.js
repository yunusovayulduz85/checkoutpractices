import React, { useState } from 'react';
import "../App.css"
import { useDispatch, useSelector } from 'react-redux';
import { addUsers, deleteUsers } from './usersSlice';
const Cashbox = () => {
    const users=useSelector((state)=>state.usersSlice);
    const [showSidebar,setShowSidebar]=useState(false);
    const [inputValue,setInputValue]=useState("");
    const [phone,setPhone]=useState()
    const dispatch=useDispatch()
    const addedUser=()=>{
        setShowSidebar(true)
    }
    const addNewUser=()=>{
        dispatch(addUsers({ id: users[users.length - 1].id + 1, name: inputValue, phone }));
        setShowSidebar(false);
    }
    const deletedUser=(id)=>{
        dispatch(deleteUsers({id:id}))
    }
    return (
        <div className='w-100 d-flex justify-content-center mt-5'>
            <div className='w-75 border border-bold p-3'>
                <h1 className='text-center'>Users</h1>
                <div className='d-flex justify-content-end'>
                    <button className='btn btn-success' onClick={addedUser}>+Add</button>
                </div>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">phone</th>
                            <th scope="col">update</th>
                            <th scope="col">delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            users.map((user,index)=>{
                                return (
                                    <tr key={index}>
                                        <th scope="row">{user.id}</th>
                                        <td>{user.name}</td>
                                        <td>{Number(user.phone)}</td>
                                        <td>
                                            <svg  xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square svg" viewBox="0 0 16 16">
                                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                            </svg>
                                        </td>
                                        <td>
                                            <svg onClick={()=>deletedUser(user.id)} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash svg " viewBox="0 0 16 16">
                                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z" />
                                                <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z" />
                                            </svg>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            {
                showSidebar && <div className='ms-5 border border-bold py-3 px-5 rounded'>
                    <div className='border border-bold rounded'>
                        <h4 className='text-start px-3'>User qo'shish</h4>
                    </div>
                    <input onChange={(e)=>setInputValue(e.target.value)}  placeholder='Enter your name' className='my-3' type='text' />
                    <input placeholder='Enter your phone' type='number' onChange={(e)=>setPhone(e.target.value)}/>
                    <div>
                        <button className='btn btn-danger' onClick={() => setShowSidebar(false)}>Chiqish</button> |
                        <button className='btn btn-info' onClick={addNewUser}>Saqlash</button>
                    </div>
                </div>
            }
        </div>
    )
}

export default Cashbox