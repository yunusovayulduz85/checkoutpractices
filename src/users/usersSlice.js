import { createSlice } from "@reduxjs/toolkit";
const initialState = [
    {
        id: 1,
        name: "Mark",
        phone: 1234567
    },
    {
        id: 2,
        name: "Jacob",
        phone: 7654321
    },
    {
        id: 3,
        name: "Larry the Bird",
        phone: 9865321
    }
]
const usersSlice = createSlice({
    name: "users",
    initialState,
    reducers: {
        addUsers: (state, action) => {
            state.push(action.payload)
        },
        deleteUsers:(state,action)=>{
            const {id}=action.payload;
            const delUser=state.find((dU)=>dU.id==id);
            if(delUser){
                return state.filter(x=>x.id!==id);
            }
        }
    }
})
export const { addUsers,deleteUsers } = usersSlice.actions;
export default usersSlice.reducer;