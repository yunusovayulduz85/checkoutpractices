import { createSlice } from "@reduxjs/toolkit";
const initialState = [
    {
        kassaName: "Kassa1",
        id: 1
    },
    {
        kassaName: "kassa2",
        id: 2
    }
]

const cashboxSLice = createSlice({
    name: "cashbox",
    initialState,
    reducers: {
        addKassa: (state, action) => {
            state.push(action.payload)
        },
        updateKassaName: (state, action) => {
            const { id, kassaName } = action.payload;
            const newKassa = state.find((x) => x.id == id);
            if (newKassa) {
                newKassa.kassaName = kassaName;
            }
        },
        deleteKassa: (state, action) => {
            const { id } = action.payload;
            const deleteID = state.find(kassa => kassa.id == id);
            if (deleteID){
                return state.filter(kassa=>kassa.id!=id)
            }
       }
    }
})
export const { addKassa, updateKassaName ,deleteKassa} = cashboxSLice.actions;
export default cashboxSLice.reducer;