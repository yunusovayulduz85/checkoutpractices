import { createSlice } from "@reduxjs/toolkit";
const time=new Date()
const initialState=[
    {
        id:1,
        userName:"Mark",
        amount:1000,
        kassa:"kassa1",
        time:time.toISOString
    }
]
const incomeSlice= createSlice({
    name:"income",
    initialState,
    reducers:{
        addUserIncome:(state,action)=>{
            state.push(action.payload);
        },
        deleteIncome:(state,action)=>{
            const {id}=action.payload;
            const delInc=state.find(f=>f.id==id)
            if(delInc){
                return state.filter(newState=>newState.id!=id)
            }
        }
    }
})
export const {addUserIncome,deleteIncome}=incomeSlice.actions;
export default incomeSlice.reducer;