import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Cashbox from '../kassa/Cashbox'
import RootLayout from '../layout/RootLayout'
import Income from '../kirim/Income'
import Remittance from '../chiqim/Cost'
import Users from '../users/Users'

const Router = () => {
  return (
    <div className='router'>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<RootLayout />}>
            <Route index element={<Cashbox />} />
            <Route path='income' element={<Income />} />
            <Route path='remittance' element={<Remittance />} />
            <Route path='users' element={<Users />} />
            <Route path='*' element={<div>Page not Found! </div>} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default Router